<?php include_once ('include/class.php');
	$dtOrder = new dtOrder;
	$dtOrder->setIdOrder($_GET['no_order']);
	$order = new COrder;
	$order->resetIdOrder($_GET['no_order']);
	$order->cekOrderByIdOrder();
	$waktu = new waktu;
	
	$queryDtOrder = mysql_query($dtOrder->cekDtOrderByIdOrder());
		
?>
<h2 class="text-center">
	Detail Order
</h2>
<div class="container">
    <hr>
    <form action="?page=updateJmlTiket&no_order=<?php echo $dtOrder->getIdOrder();?>" method="post">
	<table cellpadding="5px">
            	<tr>
                	<td>
                    	No Order
                    </td>
                    <td>
                    	:
                    </td>
                    <td>
                    	<?php echo $dtOrder->getIdOrder()?>
                    </td>
                </tr>
                <tr>
                	<td>
                    	Status Pembayaran
                    </td>
                    <td>
                    	:
                    </td>
                    <td>
                    	<?php echo $order->getStatus(); ?>  
                    </td>
                </tr>
                <tr>
                	<td>
                    	Tanggal Order
                    </td>
                    <td>
                    	:
                    </td>
                    <td>
                    	<?php echo  $waktu->format_tgl1($order->getTglOrder()); ?>  
                    </td>
                </tr>
            </table>
    <br>
    <table border="1" width="75%" align="center" cellpadding="3" style="border-collapse:collapse">
    	<tr align="center" bgcolor="#E7E7E7">
        	<th>No.</th>
            <th>Nama Event</th>
            <th>Jumlah Tiket</th>
            <th>Harga Satuan (IDR)</th>
            <th>Total Harga</th>
       </tr>
       <?php 
	   	$no = 1;
		$subtotal = 0;
		$index = 0;
	   	while($r = mysql_fetch_array($queryDtOrder)){ 
			$idTiket = $r['idTiket'];?>
       <tr>
        	<td><?php echo $no++ ?></td>
            <td><?php echo $r['nmEven'];?></td>
            <td><?php 
				if($order->getStatus() != 'Menunggu Pembayaran'){
					echo $r['jmlTiket'];
				}
				else{
					$z = $r['stok']+$r['jmlTiket'];
					?>
                    <input type="hidden" name="stokKe_<?php echo $index;?>" value="<?php echo $r['stok'] ?>">
                    <input type="hidden" name="jmlKe_<?php echo $index;?>" value="<?php echo $r['jmlTiket'] ?>">
                    <input type="hidden" name="idTiketKe_<?php echo $index;?>" value="<?php echo $r['idTiket'] ?>">
                    <input type="hidden" name="idDtOrderKe_<?php echo $index;?>" value="<?php echo $r['idDetail'] ?>">
                    <select name="jmlBaru_<?php echo $index;?>" style="width:50px;">
                    <?php
					for($i=1;$i<=$z;$i++){
						?>
                        <option
                        <?php if($r['jmlTiket'] == $i){echo "selected";} ?>><?php echo $i;?></option>
                        <?php
					}
					?>
                    </select>
                    <?php
				}
				if($order->getStatus() == 'Menunggu Pembayaran'){
				?>
                &nbsp; <a href="?page=updateJmlTiket&opt=HapusItem&idDetail=<?php echo $r['idDetail']; ?>" onClick="return confirm('Anda Yakin Tiket Tersebut Akan Dihapus dari Orderan Anda?')">Hapus Item</a>
                <?php } ?>
            </td>
            <td><?php echo number_format($r['harga']);?></td>
            <td><?php
					$total = $r['jmlTiket']*$r['harga'];
					echo number_format($total);
					$subtotal = $subtotal+$total;
			 	?>
            </td>
        </tr>
       <?php $index++;} ?>
       <tr>
       		<td colspan="4">
            	Subtotal
            </td>
            <td>
            	<?php echo number_format($subtotal);?>
            </td>
       </tr>
    </table>
    <?php
		if($order->getStatus() == 'Menunggu Pembayaran'){
			?>
            <br>
            <center>
            	<input type="hidden" name="jmlIndex" value="<?php echo $index;?>">
            	<input type="submit" value="Update Jumlah Tiket">
             </center>
                </form>
             <?php
		}
	?>
</div>