<?php
include_once("include/class.php");
$tiket = new tiket;
$tiket->setIdTiket($_GET['evenID']);
$waktu = new waktu;

if($tiket->cariRincianTiket() == '0'){
	?>
    	<script language="javascript">
			alert('Terjadi Kesalahan');
			window.location='../pembelianTiket';
		</script>
    <?php
}
else{
	?>
	<h2 class="text-center">
		<?php echo $tiket->getNmEven();
			if($tiket->getStok() <= 0){
				$status = 'disabled';
				$string = 'Tiket Habis';
			}
			else{
				$status = '';
				$string = 'Order (IDR '.number_format($tiket->getHarga()).')';
			}
		?>
	</h2>
	<div class="container">
		<hr>
		<table width="100%%" align="center">
			<tr>
				<td valign="top">
					<img src="include/uploads/<?php echo $tiket->getGambar();?>" width="500" height="250" />
					<p>
					<hr>
					<p align="center" style="padding-top:10px;"><a href="?page=tambahtiket&evenID=<?php echo $tiket->getIdTiket();?>" class="btn btn-primary <?php echo $status;?>" role="button"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> <?php echo $string; ?></a></p>
					<p align="center"><font  size="+2">Sisa <font color="red"><?php echo $tiket->getStok();?></font> Tiket</font></p>
				</td>
				<td style="padding-left:20px;" valign="top">
					<p><?php echo $tiket->getRincian()?></p>
					<p>Konser akan dilaksanakan pada <b><?php echo $waktu->format_tgl1($tiket->getAwalEven());?> pukul <?php echo $waktu->format_jam1($tiket->getAwalEven());?> s/d <?php echo $waktu->format_jam1($tiket->getAkhirEven());?></b> Waktu Setempat</p>
				</td>
			</tr>
		</table>
		
	</div>
<?php } ?>