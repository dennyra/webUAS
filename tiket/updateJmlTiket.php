<?php
	include_once ('include/class.php');
	
	$tiket = new tiket;
	$order = new COrder;
	$dtOrder = new dtOrder;
	
	if((isset($_GET['opt'])) and ($_GET['opt']=='HapusItem')){
		//Melakukan pencarian dt_order
		$idDetail = $_GET['idDetail'];
		$dtOrder->setIdDetail($idDetail);
		
		if($dtOrder->cekDtOrderByIdDetail()==1){//Melakukan pengecekan pada dt_order
			$idTiket = $dtOrder->getIdTiket();
			$jmlTiket = $dtOrder->getJmlTiket();
			$idOrder = $dtOrder->getIdOrder();
			
			//Melakukan update stok tiket (menambah tiket dengan jumlah tiket yang dikembalikan)
			$tiket->setIdTiket($idTiket);
			$tiket->cariRincianTiket();
			
			$minSubtotal = $jmlTiket*$tiket->getHarga(); //Nominal pengurang subtotal
			
			$stok_awal = $tiket->getStok();
			$stok_akhir = $stok_awal+$jmlTiket;
			$tiket->setJumlahAkhir($stok_akhir);
			
			if($tiket->updateStokById() == 1){//Jika tiket berhasil dikembalikan ke tabelnya
				//Melakukan pengurangan subtotal di tabel tb_order
				$order->resetIdOrder($idOrder);				
				if($order->cekOrderByIdOrder() != 0){
					$sub_awal = $order->getSubtotal();
					$sub_akhir = $sub_awal-$minSubtotal;
					$order->setSubtotal($sub_akhir);
					
					if($order->updateSubBaru()){//Jika subtotal berhasil diupdate, maka..
						if($dtOrder->hapusDtOrder() == 1){//Jika item yang dipilih berhasil dihapus pada tabel dt_order
							$dtOrder->setIdDetail($order->getIdOrder());
							
							if($dtOrder->cekDtOrderByIdOrder() == 0){//Jika pada order terpilih tidak ada tiket yang dipesan
								$halamanBerikutnya ='?page=orderan';//Dan akan dialihkan ke halaman orderan secara keseluruhan
							}
							else{//Jika masih ada, maka
								$halamanBerikutnya = '?page=detailorder&no_order='.$order->getIdOrder();//Halaman berikutnya adalah kembali ke detail order sebelumnya
							}
							echo $halamanBerikutnya;
							?>
                            <script>
                            	alert('Item Orderan Berhasil Dihapus');
							</script>
                            <?php
						}
					}
				}
			}
			
			
			
		}
	}
	else{
		$idOrder = $_GET['no_order'];
		$z = $_POST['jmlIndex'];
		for($i=1; $i<=$z; $i++){
			$index = $i-1;
	
			$stok = $_POST['stokKe_'.$index];
			$jmlLama = $_POST['jmlKe_'.$index];
			$idTiket = $_POST['idTiketKe_'.$index];
			$idDtOrder = $_POST['idDtOrderKe_'.$index];
			$jmlBaru = $_POST['jmlBaru_'.$index];
			
			//Melakukan update stock tiket		
			$newStok = ($stok+$jmlLama)-$jmlBaru;
			$tiket->setJumlahAkhir($newStok);
			$tiket->setIdTiket($idTiket);
			$tiket->updateStokById();
			
			//Melakukan update detail order
			$jmlTiketTambahan = $jmlBaru-$jmlLama;
			$dtOrder->setJmlTiket($jmlTiketTambahan);
			$dtOrder->setIdTiket($idTiket);
			$dtOrder->setIdOrder($idOrder);
			$dtOrder->updateJmlOrderan();
			
		}
		
		$dtOrder->setIdOrder($idOrder);
		$query = mysql_query($dtOrder->cekDtOrderByIdOrder());
		
		$subTotal = 0;
		while($r = mysql_fetch_array($query)){
			$total = $r['jmlTiket']*$r['harga'];
			$subTotal = $subTotal+$total;
		}
		$order->resetIdOrder($idOrder);
		$order->setSubtotal($subTotal);
		$order->updateSubBaru();
		$halamanBerikutnya = '?page=detailorder&no_order='.$order->getIdOrder();
	
?>
<script>
	alert('Orderan berhasil diubah.');
</script>
<?php 
	}
?>
<script>
	window.location='<?php echo $halamanBerikutnya?>';
</script>