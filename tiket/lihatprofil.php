<?php

include_once ("include/class.php");
$member = new member;
$member->setUsername($_SESSION['usernameMember']);
$member->cekUserByUsername();
$nama = $member->getNama();
$username = $member->getUsername();
$email = $member->getEmail();

	

?>
    <h2 class="text-center">
    Profil Anda
    </h2>
    <div class="container">
    <hr>
    <form method="post">
        <table style="font-size:14px" align="center" width="75%">
            <tr>
                <td colspan="3">
                    Berikut ini profil anda saat ini, untuk merubahnya silahkan menuju <a href="?page=ubahprofil">Ubah Profil</a>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <hr>
                </td>
            </tr>
            <tr>
                <td>
                    <h4>Nama Lengkap</h4>
                </td>
                <td>
                    <h4>:</h4>
                </td>
                <td>
                    <?php echo $nama?>
                </td>
                <td>
                    <span id="nama">
                    </span>
                </td>
            </tr>
            <tr>
                <td width="28%">
                    <h4>Usename</h4>
                </td>
                <td width="5%">
                    <h4>:</h4>
                </td>
                <td width="57%">
					<?php echo $username;?>
                </td>
                <td width="10%">
                    <span id="userCapt">
                    </span>
                </td>	
            </tr>
            <tr>
                <td>
                    <h4>Email</h4>
                </td>
                <td>
                    <h4>:</h4>
                </td>
                <td>
                    <?php echo $email?>
                </td>
                <td>
                    <span id="emailCapt">
                    </span>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                <hr>
                    Lakukan penggantian password secara berkala di halaman <a href="?page=ubahprofil">Ubah Profil</a> untuk menjaga performa keamanan akun anda.
                </td>
            </tr>
    </table>
    </form>
    </div>
