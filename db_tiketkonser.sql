-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 12, 2017 at 04:22 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_tiketkonser`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `idAdmin` varchar(10) NOT NULL,
  `password` varchar(8) NOT NULL,
  `nama` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`idAdmin`, `password`, `nama`) VALUES
('admin', 'password', 'Denny Ramadan');

-- --------------------------------------------------------

--
-- Table structure for table `diskon`
--

CREATE TABLE `diskon` (
  `idDiskon` int(3) NOT NULL,
  `persen` int(2) NOT NULL,
  `idTiket` varchar(11) NOT NULL,
  `awalDis` time NOT NULL,
  `akhirDis` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dt_order`
--

CREATE TABLE `dt_order` (
  `idDetail` int(11) NOT NULL,
  `idOrder` varchar(12) NOT NULL,
  `idTiket` varchar(11) NOT NULL,
  `jmlTiket` int(2) NOT NULL,
  `idDiskon` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dt_order`
--

INSERT INTO `dt_order` (`idDetail`, `idOrder`, `idTiket`, `jmlTiket`, `idDiskon`) VALUES
(38, '201611200001', '1', 2, 0),
(40, '201611200001', '2', 6, 0),
(42, '201611210001', '4', 6, 0),
(44, '201611210001', '3', 1, 0),
(45, '201611210002', '5', 1, 0),
(46, '201611250001', '2', 2, 0),
(47, '201611250002', '2', 2, 0),
(50, '201611270001', '3', 2, 0),
(51, '201611280001', '4', 1, 0),
(52, '201611280001', '6', 1, 0),
(53, '201612040001', '1', 1, 0),
(55, '201612040002', '17', 1, 0),
(57, '201612050001', '2', 1, 0),
(58, '201612050001', '1', 1, 0),
(61, '201612050002', '2', 1, 0),
(63, '201612050003', '2', 1, 0),
(64, '201612050003', '1', 1, 0),
(65, '201611200001', '4', 1, 0),
(66, '201611200001', '5', 1, 0),
(67, '201612060001', '5', 91, 0),
(68, '201612060001', '1', 1, 0),
(69, '201612060002', '5', 2, 0),
(70, '17171717M000', '1', 1, 0),
(71, '17171717J000', '5', 3, 0),
(72, '17171717J000', '1', 2, 0),
(73, '17171717J000', '1', 2, 0),
(74, '17171717J000', '1', 2, 0),
(75, '17171717J000', '1', 2, 0),
(76, '17171717J000', '2', 1, 0),
(77, '17171717J000', '1', 1, 0),
(78, '17171717J000', '1', 1, 0),
(79, '17171717J000', '5', 1, 0),
(80, '17171717J000', '1', 1, 0),
(81, '201706110001', '1', 1, 0),
(82, '201706110002', '5', 1, 0),
(83, '201706110003', '1', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `idKategori` int(2) NOT NULL,
  `nmKategori` varchar(10) NOT NULL,
  `gambar` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `username` varchar(15) NOT NULL,
  `email` varchar(40) NOT NULL,
  `noTelp` varchar(15) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`username`, `email`, `noTelp`, `password`, `nama`) VALUES
('adminasuser', 'admin@mail.com', '', 'Pass123', 'Feri Yusuf'),
('angelz_911', 'rizky@ui.ac.id', '', 'Alphabeta911', 'FAJAR RIZKI'),
('anwar', 'anwar.azulfa@gmail.com', '', 'Kemenangan1991', 'MIftakhul Anwar'),
('denny_ra', 'denny_ra@mailinator.com', '', 'Intikom11', 'Denny Ramadan'),
('feriasuser', 'feri.yusuf@hotmail.com', '', 'Pass123', 'Feri Yusuf'),
('feriyusuf', 'pass@mail.com', '', 'Pass123', 'Yusuf Feri'),
('jodiaja', 'jodi@mail.com', '', 'Password123', 'Jodi Rukmana'),
('Jody_911', 'jody@mama.com', '', 'Qwerty12345', 'JODY SURYO'),
('ririnuri', 'riri@email.com', '', 'IniPassword1', 'Riri Novelia'),
('test1', 'test@gmail.com', '', 'Pass123', 'FERRY');

-- --------------------------------------------------------

--
-- Table structure for table `tb_order`
--

CREATE TABLE `tb_order` (
  `idOrder` varchar(12) NOT NULL DEFAULT '',
  `username` varchar(15) NOT NULL,
  `tglOrder` datetime NOT NULL,
  `subtotal` double NOT NULL,
  `buktiTrf` varchar(16) NOT NULL,
  `status` enum('Menunggu Pembayaran','Pembayaran Diterima, Menunggu Verifikasi','Pembayaran Terverifikasi','Pembayaran Tidak Teverifikasi','Transaksi Dibatalkan Pembeli','Transaksi Dibatalkan Sistem','Transaksi Selesai') NOT NULL,
  `idAdmin` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_order`
--

INSERT INTO `tb_order` (`idOrder`, `username`, `tglOrder`, `subtotal`, `buktiTrf`, `status`, `idAdmin`) VALUES
('201611200001', 'feriasuser', '2016-11-20 18:00:30', 3330000, '201611200001.jpg', 'Menunggu Pembayaran', ''),
('201611210001', 'feriasuser', '2016-11-21 01:57:22', 350000, '', 'Transaksi Selesai', ''),
('201611210002', 'feriasuser', '2016-11-21 16:46:39', 50000, '', 'Transaksi Dibatalkan Pembeli', ''),
('201611250001', 'feriasuser', '2016-11-25 10:10:26', 100000, '', 'Transaksi Dibatalkan Pembeli', ''),
('201611250002', 'feriasuser', '2016-11-25 15:16:26', 100000, '201611250002.jpg', 'Transaksi Dibatalkan Pembeli', ''),
('201611270001', 'angelz_911', '2016-11-27 14:43:38', 100000, '', 'Menunggu Pembayaran', ''),
('201611280001', 'Jody_911', '2016-11-28 20:47:53', 1425000, '', 'Menunggu Pembayaran', ''),
('201612040001', 'ririnuri', '2016-12-04 20:33:49', 55000, '201612040001.jpg', 'Transaksi Selesai', ''),
('201612040002', 'ririnuri', '2016-12-04 20:38:26', 400000, '', 'Menunggu Pembayaran', ''),
('201612050001', 'feriyusuf', '2016-12-05 18:19:52', 805000, '201612050001.png', 'Transaksi Dibatalkan Pembeli', ''),
('201612050002', 'feriyusuf', '2016-12-05 18:27:27', 750000, '', 'Transaksi Dibatalkan Pembeli', ''),
('201612050003', 'feriyusuf', '2016-12-05 18:35:17', 805000, '201612050003.png', 'Transaksi Dibatalkan Sistem', ''),
('201612060001', 'test1', '2016-12-06 18:24:05', 86505000, '', 'Menunggu Pembayaran', ''),
('201612060002', 'anwar', '2016-12-06 19:27:58', 1900000, '201612060002.png', 'Transaksi Selesai', ''),
('201706110001', 'denny_ra', '2017-06-11 13:38:00', 55000, '', 'Transaksi Selesai', ''),
('201706110002', 'denny_ra', '2017-06-11 15:06:31', 950000, '', 'Transaksi Selesai', ''),
('201706110003', 'denny_ra', '2017-06-11 15:21:42', 55000, '', 'Menunggu Pembayaran', '');

-- --------------------------------------------------------

--
-- Table structure for table `tiket`
--

CREATE TABLE `tiket` (
  `idTiket` int(11) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `nmEven` varchar(50) NOT NULL,
  `rincian` text NOT NULL,
  `awalEven` datetime NOT NULL,
  `akhirEven` datetime NOT NULL,
  `harga` double NOT NULL,
  `stok` int(5) NOT NULL,
  `idKategori` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tiket`
--

INSERT INTO `tiket` (`idTiket`, `gambar`, `nmEven`, `rincian`, `awalEven`, `akhirEven`, `harga`, `stok`, `idKategori`) VALUES
(1, '_00_RAISA.jpg', 'RAISA Tour Kali Kedua', 'Konser Raisa dalam rangkaian Handmade Tour 2016 edisi Jakarta', '2016-11-01 00:00:00', '2016-11-01 00:00:00', 55000, 986, 1),
(2, '_01_NOAH.jpg', 'NOAH - HERO TOUR', 'NOAH resmi berdiri melalui konferensi pers yang digelar di Jakarta, tepatnya di Musica Studios pada tanggal 2 Agustus 2012. NOAH terdiri dari lima anggota yaitu Ariel, Uki, Lukman, Reza dan David.  NOAH diartikan sebagai panjang umur, pemberi ketenangan.Sebelumnya Ariel, Uki, Reza dan Lukman menggunakan nama Peterpan bersama dua anggota terdahulu yaitu Andika dan Indra. Sesuai kesepakatan dengan dua anggota terdahulu, maka Ariel, Uki, Lukman dan Reza tidak lagi menggunakan nama Peterpan setelah album  Sebuah Nama Sebuah Cerita.Album pertama yang dirilis menggunakan nama NOAH adalah album Seperti Seharusnya yang berisikan 10 lagu dengan single pertama Separuh Aku yang begitu videonya dirilis di Youtube telah berhasil menyedot penonton sebanyak kurang lebih 2 juta viewers dalam waktu 4 minggu.NOAH secara resmi mengangkat David sebagai personil tetap NOAH di album ini menambah warna dalam musik NOAH.Launching album Seperti Seharusnya digelar dengan cara yang tidak biasa yaitu dengan menggelar konser di 5 negara dalam waktu 24 Jam. Negara-negara yang dipilih adalah Australia, Hong Kong, Malaysia, Singapore dan Indonesia.Sebelumnya NOAH pun mengeluarkan sebuah buku Catatan 2010-2012 yang berjudul Kisah Lainnya. Kisah Lainnya mendapatkan sambutan publik yang luar biasa terbukti dengan menjadi best seller di toko buku Gramedia di berbagai kota di Indonesia.Seperti kutipan dalam buku Kisah Lainnya, ï¿½Kami akan melanjutkan perjalanan.ï¿½. Inilah awal perjalanan Ariel, Uki, Lukman, Reza dan David dengan nama baru yaitu NOAH.', '2016-11-01 00:00:00', '2016-11-02 00:00:00', 750000, 998, 1),
(3, '_03_Andra.jpg', 'Andra & The Backbone', 'Andra and the BackBone merupakan salah satu grup musik dari Indonesia. Anggotanya berjumlah tiga orang yaitu Dedy, Stevie, dan Andra Junaidi. Grup musik ini dibentuk pada tahun 2007. Lagu utamanya ialah Musnah yang dipopulerkan ulang oleh Mulan Jameela. Album pertamanya ialah Andra and the BackBone, dirilis tahun 2007. Hits lain dari album ini adalah Sempurna yang dipopulerkan ulang oleh Gita Gutawa, serta hits lainnya adalah Lagi Dan Lagi, Dan Tidurlah, Terdalam, Perih, serta Dengarkan Aku.\r\nSalah satu anggotanya, Andra Junaidi, adalah salah satu pendiri dan mantan anggota dari grup band Dewa 19.', '2016-11-03 00:00:00', '2016-11-02 00:00:00', 450000, 0, 1),
(4, '_02_JKT.jpg', 'JKT 48 - Festival Cinta', 'JKT48 adalah grup idola asal Indonesia. Dibentuk pada tahun 2011, JKT48 adalah idol group pertama di Asia Tenggara dengan fanbase mencapai lebih dari 5 juta penggemar menyebabkannya diperhitungkan sebagai salah satu grup idola yang memiliki jumlah fans terbanyak di Asia, idol grup yang lebih mengedepankan kemampuan menyanyi dan menari ini merupakan grup saudari AKB48 pertama yang berada di luar Jepang.[1] Grup ini mengadopsi konsep AKB48 yaitu \"idola yang dapat anda jumpai setiap hari\".[2] JKT48 mengadakan pertunjukan rutin hampir setiap hari di Theater JKT48, lantai 4 mal fX Sudirman, Jakarta.\r\n\r\nSampai saat ini JKT48 memiliki anggota sebanyak 71 orang[3]. Album pertama grup ini, Heavy Rotation dirilis pada 16 Februari 2013 oleh Hits Records. Singel pertama mereka, River dirilis pada 11 Mei 2013 (versi teater) dan 17 Mei 2013 (versi reguler).', '2016-11-04 00:00:00', '2016-11-02 00:00:00', 575000, 0, 1),
(5, '_05_A7X.jpg', 'AVENGED SEVEN FOLD', 'Avenged Sevenfold (sering juga ditulis A7X) adalah grup musik Hard rock Amerika Serikat yang dibentuk pada tahun 1999. Grup musik ini berasal dari Huntington Beach, California. Anggota Avenged Sevenfold pada saat ini terdiri dari M. Shadows, Synyster Gates, Zacky Vengeance, Johnny Christ, dan Arin Ilejay.Band ini dikenal dengan genre Metalcore pada debut mereka Sounding the Seventh Trumpet, yang mengandung banyak vokal scream. Band ini mengubah gaya mereka di album ketiga mereka, City of Evil, yang menampilkan vokal melodis dan power ballad. Band ini terus mengeksplorasi suara baru dengan mengeluarkan yang berjudul Avenged Sevenfold dan menikmati kesuksesan sebelum drummer mereka, James ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 950000, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`idAdmin`);

--
-- Indexes for table `diskon`
--
ALTER TABLE `diskon`
  ADD PRIMARY KEY (`idDiskon`);

--
-- Indexes for table `dt_order`
--
ALTER TABLE `dt_order`
  ADD PRIMARY KEY (`idDetail`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`idKategori`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `tb_order`
--
ALTER TABLE `tb_order`
  ADD PRIMARY KEY (`idOrder`);

--
-- Indexes for table `tiket`
--
ALTER TABLE `tiket`
  ADD PRIMARY KEY (`idTiket`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `diskon`
--
ALTER TABLE `diskon`
  MODIFY `idDiskon` int(3) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dt_order`
--
ALTER TABLE `dt_order`
  MODIFY `idDetail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `idKategori` int(2) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tiket`
--
ALTER TABLE `tiket`
  MODIFY `idTiket` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
